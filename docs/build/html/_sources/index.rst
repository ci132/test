.. pipeline documentation master file, created by
   sphinx-quickstart on Thu Aug  4 09:14:17 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pipeline's documentation!
====================================

This is a sample documentation for **GWAS Pipeline** 


.. note::

   This project is only for testing.

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
